﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibraryRepository.Controller;
using ClassLibraryRepository;
using log4net;

namespace WEB_API.Controllers
{
    public class usersController : ApiController
    {
        private static readonly ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);     
        private IUserRepository IuserRepo;
        public usersController(IUserRepository userRepository)
        {    
            IuserRepo = userRepository;
        }
        // GET: api/users
        public IQueryable<user> Getusers()
        {
            Log.Error("Get Error");
            return IuserRepo.GetAll();
        }

        // GET: api/users/5    
        [ResponseType(typeof(user))]
        public async Task<IHttpActionResult> Getuser(int id)
        {

            user usr = await IuserRepo.GetByIdAsync(id);
            if (usr == null)
            {
                Log.Error("Get by id Error");
                var notFoundResponse = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("UserID was not found")
                };

                throw new HttpResponseException(notFoundResponse);
            }
            
            return Ok(usr);
        }

        // PUT: api/users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putuser(int id, user usr)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usr.id)
            {
                Log.Error("Update Error");
                return BadRequest();
            }
            await IuserRepo.EditAsync(usr);
            try
            {
               await IuserRepo.SaveChangesAsync(usr);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return StatusCode(HttpStatusCode.Created);
        }

        // POST: api/users
        [ResponseType(typeof(user))]
        public async Task<IHttpActionResult> Postuser(user usr)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Log.Error("Create Error");
            await IuserRepo.InsertAsync(usr);


            return CreatedAtRoute("DefaultApi", new { id = usr.id }, usr);
        }

        // DELETE: api/users/5
        [ResponseType(typeof(user))]
        public async Task<IHttpActionResult> Deleteuser(int id)
        {

            user usr = await IuserRepo.FindAsync(id);
            if (usr == null)
            {
                Log.Error("Delete Error");
                var notFoundResponse = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("UserID was not found")
                };

                throw new HttpResponseException(notFoundResponse);
            }

            await IuserRepo.DeleteAsync(usr);
            return Ok(usr);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        IuserRepo.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        
    }
}